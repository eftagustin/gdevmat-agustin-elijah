Mover mover;
Mover[] movers = new Mover[5];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.mass = 10;
  //mover.acceleration = new PVector(0.1, 0);
  
  for (int i = 0; i < 5; i++)
  {
    movers[i] = new Mover(Window.left + 50, 200);
    movers[i].mass = i + 1;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1f, 0);


void draw()
{
  background(255);
  
  noStroke();
  for (Mover m : movers)
  {
    m.applyGravity();
    
    m.applyForce(wind);
    m.render();
    m.update();
    
    if(m.position.x > 0){
    
      m.applyFriction(0.95); 
    }
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
  }
  

}
