public class Vector2{
 public float x;
 public float y;
  
  Vector2 (){
    this.x = 0;
    this.y = 0;
  }
  
  Vector2 (float x, float y){
   this.x = x;
   this.y = y;
  }
  
  public void add(Vector2 v){
    this.x += v.x;
    this.y += v.y;

  }
  
}
