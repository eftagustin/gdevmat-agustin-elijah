public class BlackHole
{
   public PVector position;
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   BlackHole()
   {
      position = new PVector(); 
   }
   
      
   public void render()
   {
      noStroke();
      fill(r,g,b,a);
      circle(position.x, position.y, scale); 
   }
   
     BlackHole(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
      
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
}
