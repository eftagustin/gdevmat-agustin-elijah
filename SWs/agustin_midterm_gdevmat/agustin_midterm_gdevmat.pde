void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  populateMovers();
  setupMovers();
}

//mover[100] = new Mover[];

Mover mover = new Mover();
BlackHole blackHole = new BlackHole();
Window window = new Window();

Mover[] moverArray = new Mover[101];

void populateMovers(){
      //MOVER
     //array populate
     for(int j = 0; j < 101; j++){
         moverArray[j] = new Mover(0,0);
       
     }
}

void setupMovers(){

    //mover position
          for(int i = 0; i <= 100; i++){
         //loop array call
              float factor = randomGaussian();
              float meanHor = random(Window.left, Window.right);
              float std = 1500;
              float meanVer = random(Window.bottom, Window.top);
              
              float x = std*factor + meanHor;
              float y = std*factor + meanVer;
              
              
                
              moverArray[i] = new Mover(x,y,random(20,50));
              moverArray[i].setColor(random(155), random(25), random(155), random(255));
      
            }
       
       if(frame%100 == 0){
            blackHole = new BlackHole(random(Window.left,Window.right), random(Window.bottom, Window.top), 50);
            
                    
            println("x =" + blackHole.position.x + " y =" + blackHole.position.y);
    }
  
    //needs to be white
    blackHole.setColor(255,255,255,255);
}

int frame = 0;
int canMove = 1;

void draw()
{
    background(0);
  
    //reposition
    frame++;
  
    println("frame= "  + frame);


  
      for(int i = 0; i <= 100; i++){
         moverArray[i].render();
         PVector dir = PVector.sub(blackHole.position, moverArray[i].position);
         dir.normalize().mult(10);
         moverArray[i].position.add(dir);
      }
      
         blackHole.render();

  
    if(frame >= 100){
        frame = 0;
        setupMovers();
        frameCount = -1;
    }
}
