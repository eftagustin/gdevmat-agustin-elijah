Mover mover = new Mover();

void setup(){
    size(1920, 1080, P3D);
    camera(0,0, Window.eyeZ, 0,0,0,0,-1,0);
    
//   mover = new Mover();
//    mover.position.x = Window.left + 50;
//    mover.acceleration = new PVector(0.2,0);
    //mover.velocity = new PVector(10,20);
     
      populateMovers();
      setupMovers();
}

Mover[] moverArray = new Mover[11];

void populateMovers(){
      //MOVER
     //array populate
     for(int j = 0; j < 11; j++){
         moverArray[j] = new Mover(0,0);
     }
}

void setupMovers(){

  
    mover.position.x = Window.left + 50;
    mover.acceleration = new PVector(1,0);
    //mover.velocity = new PVector(10,20);
  
    //mover INITIAL position
          for(int i = 0; i < 11; i++){
         //loop array call
              
              float x = Window.left + 50;
              float y = Window.bottom + 500;
              float scale = i*20;

              //fill(random(155), random(155), random(155), random(255));
              moverArray[i] = new Mover(x, y, scale, i);   
              moverArray[i].setColor(random(25), random(255), random(155), 100);
              moverArray[i].acceleration = new PVector(0.5,0);
            }
}

//second law of motion
//even first law, since we applied an unbalanced force
PVector wind = new PVector(0.1f,0);
PVector gravity = new PVector(0, -0.98);

void draw()
{
    background(255);    
    
   for(int i = 0; i < 11; i++){
      moverArray[i].render();

    //it can only reach here
      moverArray[i].velocity.limit(30);
    
      moverArray[i].applyForce(wind);
      moverArray[i].applyForce(gravity);

  
  if( moverArray[i].position.y < Window.bottom){
      moverArray[i].velocity.y *= -1;
      moverArray[i].position.y = Window.bottom;
  
        }
        
  if ( moverArray[i].position.x > Window.right){
       moverArray[i].velocity.x *= -1;
       moverArray[i].position.x = Window.right;
        }
        
   if ( moverArray[i].position.x < Window.left){
       moverArray[i].velocity.x *= -1;
       moverArray[i].position.x = Window.left;
        }
        
    if ( moverArray[i].position.y > Window.top){
       moverArray[i].velocity.y *= -1;
       moverArray[i].position.y = Window.top;
        }
 
   }
   

  
}
