public class Mover{
    public PVector position = new PVector();
    public PVector velocity = new PVector();
    public PVector acceleration = new PVector();
    public float r = 255, g = 255, b = 255, a = 255;
    
    public float mass = 1;
    public float scale = 10;
    
   Mover()
   {
      position = new PVector(); 
   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale, float mass)
   {
      position = new PVector(x, y);
      this.scale = scale;
      this.mass = mass;
   }
   
    
    public void render()
    { 
        //updates necessary variables every frame
        //render calls update
        update();
        noStroke();
        fill(r,g,b,a);
        
        //draw circle every frame
        circle(position.x, position.y, scale);
    }
    
    private void update()
    {

      this.velocity.add(this.acceleration);

      //add velo to pos per frame
      this.position.add(this.velocity);
      
      //to prevent bloat of accel per frame, avoid overvalue
      this.acceleration.mult(0);
    }

public void applyForce(PVector force){
       //a = f/m; 
      PVector f = PVector.div(force, this.mass);
      this.acceleration.add(f);
}

   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }

}
