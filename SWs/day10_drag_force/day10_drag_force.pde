Mover mover;
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
Mover[] movers = new Mover[10];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
    mover = new Mover();
    mover.position.x = Window.left + 30;
    mover.mass = 1;
    
  for (int i = 0; i < 10; i++){
    movers[i] = new Mover(Window.left + i*100, 200);
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    movers[i].mass = 10 - i;
  }
}

PVector wind = new PVector(0.25f,0);

void draw()
{
  background(255);
  
  ocean.render();
  noStroke();
  
  for (Mover mover: movers)
  {

    mover.applyGravity();
    mover.applyFriction(0.2);
    
   if(mover.position.y > 0){
     mover.applyForce(wind);
   }
   
    mover.render();
    mover.update();
    
    if (mover.position.x > Window.right)
    {
      mover.velocity.x *= -1;
      mover.position.x = Window.right;
    }
    
    if (mover.position.y < Window.bottom)
    {
      mover.velocity.y *= -1; 
      mover.position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(mover))
    {
      mover.applyForce(ocean.calculateDragForce(mover)); 
    }
  }

}
