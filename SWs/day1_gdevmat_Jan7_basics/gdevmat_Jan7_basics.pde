void setup(){
  //called for init, first frame
  //p3d makes env 3d
  
   size(1920,1080,P3D);
 
  //if win (1080,720,P3D);
  
  camera(0,0,-(height/2)/tan(PI*30/180), //cam position
         0,0,0, //eye position
         0, -1, 0); //up vector

}

int y;

void draw(){
  //renders every frame, AFTER setup
  
  //draw a circle
  
  background(130);
  //circle(0,y,100);
  //y++;
  
  //we can move the camera so stuff and origin are in middle
  //here, Y goes down
  //needs flipping
  
  //circle is only constant if it displays every frame
  
  //C++ is top down
  //but java is not
  
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  radius++;
  
  drawSineFunction();
  c++;
}

void drawCartesianPlane(){
 line(300, 0, -300, 0); 
 line(0, 300, 0, -300);
 
 for(int i = -300; i <= 300; i += 10){
   line(i, -5,  i, 5);
   line(-5, i, 5, i);
 }
}

void drawLinearFunction(){
 for (int x = -200;x<=200;x++){
  circle(x, x+2, 1); 
 }
}
 
 void drawQuadraticFunction(){
   /* f(x)= 10x^2 + 2x -5
   */
   
   for(float x = -300; x <= 300; x+=0.1){
     circle(x*10, (float)Math.pow(x, 2) + (2*x) - 5, 1);
   }
 }
 
 void drawCircle(){
   for(int x = 0; x<360; x++){
       circle((float)Math.cos(x)*radius, (float)Math.sin(x)*radius, 1);
      
   }
 }
 
 float a = 20;
 float b = 1;
 float c = 0.8;
  
  //amp, freq
  void drawSineFunction(){
    for(float x = -300; x < 300; x+=0.1){  
    circle(x*5, a*(float)Math.sin(b*x+ c),1);
    
    }
  }
  float radius = 50;
