
void setup(){
  //called for init, first frame
  //p3d makes env 3d
  
   size(1920,1080,P3D);
 
  //if win (1080,720,P3D);
  
  camera(0,0, Window.eyeZ, //cam position
         0,0,0, //eye position
         0, -1, 0); //up vector

  //background(234, 235, 94);
}


Vector2 position = new Vector2();

Vector2 velocity = new Vector2(5,8);

void draw(){ 
 //clear every frame 
 background(234, 235, 94);
  
position.add(velocity);
  
  if(position.x > Window.right || position.x <Window.left){
    velocity.x *= -1;    
    
  }
  
  if(position.y> Window.top || position.y < Window.bottom){
    
    velocity.y *=-1;
  }
  
  
 fill(181);
 noStroke();
 circle(position.x,position.y,40); 
  
}
