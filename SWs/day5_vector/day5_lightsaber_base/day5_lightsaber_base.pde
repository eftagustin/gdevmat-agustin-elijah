
void setup(){
  //called for init, first frame
  //p3d makes env 3d
  
   size(1920,1080,P3D);
 
  //if win (1080,720,P3D);
  
  camera(0,0, Window.eyeZ, //cam position
         0,0,0, //eye position
         0, -1, 0); //up vector

  //background(234, 235, 94);
}


Vector2 mousePos(){
  
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
return new Vector2(x,y);
}

void draw(){ 
 //clear every frame 
 background(0);
 
 Vector2 mouse = mousePos();
 
 mouse.normalize();
 mouse.mult(500);
 
 strokeWeight(15);
 stroke(random(255),random(255),random(255),255);
 line(0,0, mouse.x, mouse.y);
 line(0,0, -mouse.x, -mouse.y);
 
 
 strokeWeight(10);
 stroke(random(0),random(170)+85,random(0),255);
 line(0,0, mouse.x, mouse.y);
 line(0,0, -mouse.x, -mouse.y);
 
 
 strokeWeight(20);
 stroke(50,50,50,255);
 line(0,0, mouse.x/7, mouse.y/7);
 line(0,0, -mouse.x/7, -mouse.y/7);
// line(0,0, mouse.x+90, mouse.y+90);
  
   
 strokeWeight(20);
 stroke(80,80,80,255);
 line(0,0, mouse.x/7, mouse.y/7);
// line(0,0, mouse.x+90, mouse.y+90);
  
  println("Maggy: " + mousePos().mag());
}
  
  
 //fill(181);
 //noStroke();
 //circle(position.x,position.y,40); 
  
