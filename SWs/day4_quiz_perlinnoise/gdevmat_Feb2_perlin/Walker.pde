class Walker{
  float xPosition;
  float yPosition;
  
  float time = 0.01;
  float nY;
  float nX;
  float nS;
 // float nC;
  
  //float c = 255;
  float x = 10;
  float y = 10;
  float size = 30;
  
    float r_t = 0;
    float g_t = 0;
    float b_t = 0;
    
    float nR;
    float nG;
    float nB;

    float red;
    float green;
    float blue;
  
  void render(){
    
    nS = noise(time);
    nS = map (nS, 0, 1, 30, 120);
   // randomColor(); 
    
    noStroke();
    circle(xPosition, yPosition, nS);
    time+=0.05;
   
   walker.randomColor();
  }
  
  
  void randomWalk(){
   //heads fwd
   //tails bwd
   //left
   //right
    
   int decision = floor(random(8));
    
    nX = noise(time);
    x = map(nX, 0, 1, 0.1, 15);
    
    nY = noise(time);
    y = map(nY, 0, 1, 0.1, 15);
    
    if(decision == 0){
     yPosition+= y;
    }
    else if(decision ==1){
     yPosition -= y;
    }
      else if(decision ==2){
     xPosition -= x ;
    }
      else if(decision ==3){
     xPosition += x ;
    }
      else if(decision ==4){
     yPosition -= y;
     xPosition -= x ;
    }
      else if(decision ==5){
     yPosition -= y;
     xPosition += x ;
    }
      else if(decision ==6){
     yPosition += y ;
     xPosition += x  ;
    }
      else if(decision ==7){
     yPosition += y ;
     xPosition -= x ;
    }
    
     time+=0.01;
     println("y" + y);
      println("x" + x);
  }
  
  void randomColor(){
    //nC = noise(time);
    //nC = map (nC, 0, 1, 0, 255);
    
    r_t = 19;
    g_t = 92;
    b_t = 67;
    
    //nR = noise(r_t);
    //nG = noise(g_t);
    //nB = noise(b_t);

    //plugging in r_t,g_t,b_t doesn't change values to reflect Perlin
    //so put in time, which works as in Size and X and Y values
    //but modified by diff modifiers

    red = map(noise(time*0.5), 0, 1, 0, 255);
    green = map(noise(time*0.75), 0, 1, 0, 255);
    blue = map(noise(time), 0, 1, 0, 255);
    
    fill(red,green,blue,255);
    
    time+=0.001;
    
    println("Red" + red);
        println("Blue" + blue);
            println("Green" + green);
  }
}
