class Splat
{
  float mean = 0;
  float std = 350;
  float stdScale = 175;
  float gauss = randomGaussian();

  void SplatGen() {

    float x = std*gauss + mean;
    float y = random(-20, 20)*std + mean;
    float scale = gauss*(stdScale);

    noStroke();
    fill(random(255), random(255), random(255), 20);
    circle(x, y, scale);

    ++frameNo;
    println(frameNo);
    Clear();
  }

  void Clear() {

    if (frameNo == 100) {
      println("1000");
      fill(255);
      //rect(0,0,width,height);
      background(255);
    }
  }
}
