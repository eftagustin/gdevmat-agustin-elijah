void setup() {
  //called for init, first frame
  //p3d makes env 3d

  size(1920, 1080, P3D);
  background(0);

  //if win (1080,720,P3D);

  camera(0, 0, -(height/2)/tan(PI*30/180), //cam position
    0, 0, 0, //eye position
    0, -1, 0); //up vector
}

int frameNo;

void draw() {
  Splat splat = new Splat();
  splat.SplatGen();
}
