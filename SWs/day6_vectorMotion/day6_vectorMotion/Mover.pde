public class Mover{
    public PVector position = new PVector();
    public PVector velocity = new PVector();
    public PVector acceleration = new PVector();
    
    public float scale = 50;
    
    public void render()
    { 
        //updates necessary variables every frame
        update();
        
        //draw circle every frame
        circle(position.x, position.y, scale);
    }
    
    private void update()
    {

      //add accel to velo every frame
      if(this.position.x <= 0){ 
        this.velocity.add(this.acceleration);
          println("am adding");
      }
      else if(this.position.x > 0){
        fill(69,4,20);
        circle(position.x, position.y, scale);
        
        this.velocity.sub(this.acceleration);
           println("am subbing");
      }
      
      println("x = " + this.position.x + " v = " + this.velocity.x);
      
      if(this.velocity.x < 0.5)
      {
        this.velocity.x = 1;
      }
      
      //constrain(this.velocity.x, 0.00005, 10);
      
      //add velo to pos per frame
      this.position.add(this.velocity);
    }
}
